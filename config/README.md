# SBOS Configs
## Edit these files to see the change propogate on a rebuild

This dirrectory mainly serves as the `src` for builder deverivatives of configurations when rebuilding Nix. From there, they are symlinked to the appropriate location.

## Pre-configured tools:
---
#### [Git](./base/gitconfig)
- Uses Delta for diffs
- TODO:
  * Add Git-LFS by default
  * Add git-annex by default
---
#### [Fish](./base/config.fish)
- Uses McFly for histroy. 
  * Uses Starship.rs for prompt. 
  * Aliases `ls` and `ll` to `exa --icons` and `exa -l --icons` respectivly
---
#### [starship](./base/starship.toml)
- There is way too much going on here to discribe. Have fun! 
---
#### [Plasma](./plasma)
- Heavily inspired by Garuda. 
- Another MacOS-like Plasma config is comming soon. 

