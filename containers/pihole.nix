{config, pkgs, ...}:
{
  virtualisation.oci-containers.containers."pihole" = { 
    image = "pihole/pihole:latest"; 
    #dependsOn = [ "unbound" ]; 
    ports = [ "53:53/tcp" "53:53/udp" "67:67/udp" "80:8080/tcp" "443:443/tcp" ]; 
    environment = { 
      TZ = "America/Denver"; 
      WEBPASSWORD = "2345"; 
      DNS1 = "9.9.9.9";
      DNS2 = "149.112.112.112";
      DNSSEC = "true";
      WEBTHEME = "default-dark";
      WEB_BIND_ADDR = "100.77.211.51";

    }; 
    volumes = [ "pihole:/etc/pihole" ]; 
    extraOptions = [ 
    #"--network=pihole-unbound" 
    #"--ip=172.19.0.2" "--net=host" 
    ]; 
  }; 
}
