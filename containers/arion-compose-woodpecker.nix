{
  services = {
    woodpecker-server = {
      service.image = "woodpeckerci/woodpecker-server:latest";
      service.volumes = [ "${toString ./.}woodpeckerci/woodpecker-server:latest" ];
      service.ports = [ "8000:8000" ];
      service.environment = {
        WOODPECKER_OPEN="true";
        WOODPECKER_HOST="WOODPECKER_HOST";
      };
    };
    woodpecker-agent.service = {
      image = "woodpeckerci/woodpecker-agent:latest";
      command = "agent";
      restart = "always";
      depends_on = ["woodpecker-server"];
      volumes = [ 
        "${toString ./.}/var/run/podman/podman.sock:/var/run/podman/podman.sock" 
         "${toString ./.}/var/run/podman/podman.sock:/var/run/docker.sock" 
      ];
      environment = {
        WOODPECKER_SERVER="woodpecker-server:9000";
        WOODPECKER_AGENT_SECRET="123456";
      };
    };
  };
  #volumes =  [ "woodpecker-server-data:"]; 
}
