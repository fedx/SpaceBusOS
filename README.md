# SpaceBusOS

## THIS IS A WIP, PROCEDE WITH CAUTION.

---

### How This works:

This is a sort of unified NixOS config that installs via shell scripts. Currently this only supports my config for my Galago Pro, listed under `devices/galp.nix`. The install script will coppy the nessecary files to `/etc/nixos` and rebuild the system as a way of creating an identical setup to my Galago. I have also included my Plasma, Fish, and Bash configs which install to `/nix/store` and are symlinked to your home dirrectory.

If you have no idea what is going on, please read up on NixOS and Linux. The [Nixos Wiki](https://nixos.wiki) and [Arch Wiki](https://wiki.archlinux.org) are my two favorite resources. 


### Quickstart:

**THIS IS AN UNTESTED SCRIPT! PROCEED WITH CAUTION!**

---
#### New installs:
- Download a NixOS [iso](https://nixos.org/download.html).
- Live boot the ISO.
- Format your disk and mount your root partition to `/mnt` and your boot partition to `/mnt/boot`. See NixOS [docs](https://nixos.org/manual/nixos/stable/index.html#sec-installation).
- Run `curl -O https://codeberg.org/fedx/SpaceBusOS/raw/branch/master/install.sh` then `chmod +x install.sh`
- To run the install script, run `./install.sh --live`.
- Customise the configs in `/mnt/etc/nixos` to your liking, then run `sudo nixos-install`
- Reboot into a NixOS system!
### TODO:
- [ ] Make install script `curl`able.
- [ ] Add more default device configs.
- [ ] Include support for device configswitching in the install script.
- [ ] Add all my configs including Plasma, Sway, Zsh, and much more.
- [ ] Clean up the code base.
- [ ] Add auto addition of configs with an installed package
- [ ] Port more hacking packages over to `packages/blacknix`
- [ ] SCRIPT ALL THE THINGS!!!!
---
### FAQ:

#### Why SpaceBus?

I was talking with a feind about space travel (as one does), and I mentioned the idea of using a "Space Bus."
#### Is it not an overstatment to call this an OS?

Yes, my orriginal plans where a lot bigger, and I thought it would be more of an OS. As is, this is barrley a respin.

## I have a very bad [blog](./blog/README.md) talking about stuff I have done.

---

### Blacknix
[Blacknix](./packages/blacknix) is the codename I use for the sub-project I have started where I port and curate pentesting applications. 

### Services: 
I have packaged and preconfigured several [services](./services) for your use. 

### Config:
I have spent a lot of time making a lot of [configs](./config). 

### Silence: 
The codebase is an MPL license, and the desktop images are CC-BY-NC. AI training on any of this project is not welcome. 

