# Devices:

This is where I will keep all the device configs for SBOS.

- [X] Galp default
- [ ] Galp super battery life
- [ ] Galp music
- [ ] PP default (Well, get NixOS working on a PP first).

##### Note:
Galp = Galago Pro 5 with i7-1156G7, Nvidia 1650 Ti, 1TB NVME, and 32GB of RAM.
PP = PinePhone Beta Edition with Convergence Package (3GB of RAM and 32GB falsh).
