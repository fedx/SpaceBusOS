{ config, pkgs, inputs, ... }:

{
  imports = [
    ./packages/neovim.nix
  ];
  hardware.enableRedistributableFirmware = true;
  system.stateVersion = "24.05";
  time.timeZone = "America/Denver";
  nix = {
    #package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
#  system.autoUpgrade = {
#    enable = true;
#    flake = inputs.self.outPath;
#    flags = [
#      "--update-input"
#      "nixpkgs"
#      "-L" # print build logs
#    ];
#    dates = "02:00";
#    randomizedDelaySec = "45min";
#  };
  nix.nixPath = [ "nixpkgs=${inputs.nixpkgs}" ];
  environment.systemPackages = with pkgs; [
    nix-index
    parted
    nmap
    gptfdisk
    comma
    fish
    hwinfo
    iw
    pciutils
    duf
    unar
    bottom
    btop
    mtr
    glow
    tealdeer
    wget
    nixos-generators
    git
    distrobox
    git-lfs
    usbutils
    delta
    neofetch
    btrfs-progs
    bandwhich
    gping
    fd
    restic
    rclone
    ripgrep
    tealdeer
    eza
    neofetch
    bat
    starship
    dogdns
    procs # System procecies
    gum # It makes scripts look perty
    killall

  ];
  zramSwap.enable = true;
  programs.zsh = {
    enable = true;
    syntaxHighlighting.highlighters = ["main" "brackets" "pattern" "cursor" "regexp" "root" "line"];
    enableCompletion = true;
    ohMyZsh = {
      enable = true;
      theme = "robbyrussell";
    };
    loginShellInit="neofetch -l";
  };


  environment.shellAliases = {
    ll = "eza -l --icons --sort date"; 
    ls = "eza --icons --sort date";
    icat = "kitty +kitten icat";
    ssh = "kitty +kitten ssh";
    cat = "bat";
    grep = "rg";
  };
  environment.variables = {
    EDITOR="nvim";
  };
}
