{ config, pkgs, ... }: {
  users.users.fedx = {
    isNormalUser = true;
    initialPassword = "SPACE";
    shell = pkgs.zsh;
    extraGroups = [
      "networkmanager"
      "wheel"
      "kvm"
      "podman"
      "video"
      "audio"
      "kismet"
      "wireshark"
      "syncthing"
      "plugdev"
      "input"
      "uinput"
      "mpd"
    ];
  };
  programs.zsh.enable = true;
  #   users.users.test = {
  #     isNormalUser = true;
  #     initialPassword = "SPACE";
  #   };
#     security.pam = {
#        mount = {
#          enable = true;
#          extraVolumes = [ "<volume user='fedx' fstype='fuse' options='nodev,nosuid,quiet' path='${pkgs.gocryptfs}/bin/gocryptfs#/home/%(USER)/cipher' mountpoint='/home/%(USER)/plain' />"];
#       };
#    };
  # #     u2f = {
  # #       enable = true;
  # #       cue = true;
  # #       authFile = /etc/u2f-mappings;
  # #       control = "sufficient";
  # #       interactive = true;
  # #     };
  #    };
  #    environment.systemPackages = [
  #      pkgs.gocryptfs
  #    ];
  #    system.activationScripts.gocryptfs.text = ''
  #      ln -nsf ${pkgs.gocryptfs}/bin/gocryptfs /usr/bin
  #    '';
}
