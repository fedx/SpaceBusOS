# BlackNix
This is my personal project for porting pentesting tools to NixOS. Some of these are in NixPKGs, and some I have not moved over due to bugs listed in the `package.nix` file.
Also see [Are We Hackers Yet?](https://jjjollyjim.github.io/arewehackersyet/index.html)
---
## Available packages:

- [x] 0trace (packaged here, has issues)
- [X] abootimg (in NixOS)
- [X] aesfix (merged, not built in cache yet)
- [X] aeskeyfind (awaiting merge)
- [X] afflib (in NixOS)
- [X] aflplusplus (in NixOS)
- [X] aircrack-ng (in NixOS, buggy)
- [ ] airgeddon
- [ ] altdns
- [ ] amap (needs more reasurch)
- [X] amass (in NixOS)
- [ ] android-sdk
- [X] Apache (in NixOS)
- [X] apktool (in NixOS)
- [ ] arjun
- [ ] armitage
- [X] arp-scan (in NixOS)
- [X] apring (in NixOS)
- [ ] arpwatch
- [ ] asleep
- [X] atftp (in NixOS)
- [ ] autopsy
- [X] axel (in NixOS)
- [ ] backdoor-factory
- [ ] bed
- [ ] beef-xss (WIP, needs to be ported, will be next merge)
- [ ] berate-ap
- [X] bettercap (In NixOS)
- [ ] bind9
- [ ] bing-ip2hosts
- [ ] binwalk
- [ ] bloodhound
- [ ] bluelog
- [ ] blueranger
- [ ] bluesnarfer
- [ ] bluez
- [ ] braa
- [ ] bruteforce-salted-openssl
- [ ] bruteforce-wallet
- [ ] brutepsuite
- [ ] btscanner
- [ ] bully
-
- [ ] CrackmapExec (WIP, Python is a pain)
