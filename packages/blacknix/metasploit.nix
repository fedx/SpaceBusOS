{ config, pkgs, fetchurl, lib, ... }:
{
  services.postgresql.enable = true;
  environment.systemPackages = [ pkgs.metasploit ];
}
