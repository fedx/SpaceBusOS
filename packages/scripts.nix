{ config, pkgs, ... }:
let
  RepoPath = "/home/krutonium/NixOS";
  RepoURL = "gitea@gitea.krutonium.ca:Krutonium/My_Unified_NixOS_Config.git";

  update = pkgs.writeShellScriptBin "up-cfg" ''
    echo Updating Local System
    cd ${RepoPath}
    git pull
    if [[ -f /etc/nixos/configuration.nix ]]; then
      sudo cp ../common.nix /etc/nixos/configuration.nix
      sudo cp ../devices/galp.nix /etc/nixos/dev.nix
      sudo cp -r ../bootloaders/ /etc/nixos/
      sudo cp -r ../packages.nix /etc/nixos/
    else
      #TODO
      echo "function TBD"
    fi

    updatePackages
  '';
  updatePackages = pkgs.writeShellScriptBin "up" ''
    if [[ -f /etc/nixos/configuration.nix ]]; then
      sudo nixos-rebuild switch --upgrade
      nix-channel --update
      nix-env -u
      home-manager switch
      sudo nix-collect-garbage --delete-older-than 7d
    elif yay --help; then
      yay -Syu --no-confirm
    elif pacman --help; then
      sudo pacman -Syu --no-confirm
    fi
    tldr -u
  '';
in
{
  environment.systemPackages = [ update updatePackages ];
}
