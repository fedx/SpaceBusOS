{config, pkgs, ...}:{
  programs.wireshark.enable = true;
  environment.systemPackages = with pkgs; [
    termshark
    wireshark
    tshark
  ];
}
