 { pkgs, config, lib, ... }:
let
  version = "1.0.0.beta.2.1.1";

  zrythm-appimage = pkgs.appimageTools.wrapType2 {
  name = "zrythm-appimage";
  src = pkgs.fetchurl {
    url = "https://www.zrythm.org/downloads/zrythm-trial-1.0.0.beta.2.1.1-x86_64.AppImage";
    sha256 = "sha256-xkBQXOOeD/HuUzhh/mEILttD4s9CvX3ou8Bj1xtAHrw=";
  };
  };
in
{
  environment.systemPackages = [zrythm-appimage pkgs.libxcrypt];
}

