{ config, pkgs, ...}:
{
  imports =
  [
    ./blacknix/zero_trace.nix
    ./blacknix/metasploit.nix
    ./blacknix/ghidra.nix
  ];
  environment.systemPackages = with pkgs; [
    abootimg
    acsccid
    #aesfix
    #aeskeyfind
    aespipe
    afflib
    aflplusplus
    aircrack-ng
    amass
    apktool
    arp-scan
    arping
    atftp
    axel
    bettercap
    cifs-utils
    dogdns
    #ftp
    iw
    lvm2
    mlocate
    libressl
    nfs-utils
    p7zip
    parted
    #rfkill
    #net-nnmp
    tcpdump
    testdisk
    #netkit-tftp
    unrar
    nmap
    kismet
    wireshark
    burpsuite

  ];
}
