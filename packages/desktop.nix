{config, pkgs, ...}: {
  services.udev.packages = with pkgs; [ numworks-udev-rules android-udev-rules qmk-udev-rules ];
  environment.systemPackages = with pkgs; [
    mp3blaster
    #stab-blender.blender
    blender
    tor-browser
    dune3d
    libreoffice
    ollama
    yt-dlp
    mepo
    xournalpp
    waypipe
    rnote
    qucs-s
    ngspice
    unciv
    #unstable.ladybird
    ffmpeg
    obs-studio
    kdenlive
    android-tools
    #toybox
    (pkgs.callPackage ./qcsuper.nix {} )
    valgrind

    darktable
    prusa-slicer
    mpv
    ssh-tools
    #speedtest-cli
    #librecad
    #nmap
    brightnessctl
    #kicad-unstable-small
    #stable.blender
    #gtkcord4
    inkscape
    keepassxc
    protonmail-bridge
    krita
    #session-desktop
    #openscad
    #horizon-eda

    hexyl
    #imhex

    ####################
    ## World Wide Web ##
    ####################
    #rdrview
    #lynx
    #gomuks
    librewolf-wayland
    thunderbird

    sc-im
    iamb
    unoconv
    whois
    texliveTeTeX



    ############################
    ## Software Defined Radio ##
    ############################
    sdrpp
    # cubicsdr
    # gqrx
    # sdrangel
    # urh
    # dsd
    hackrf
    rtl-sdr
    # gnuradio

   


    #########
    # Audio #
    #########
    qpwgraph
    sonic-pi
    mixxx

    ############
    # Graphics #
    ############
    #blender
    #krita
    #inkscape
    # prusa-slicer

    ##########
    # Office #
    ##########
    pandoc
    #libreoffice
    librsvg
    #texlive

    ###############
    # Development #
    ###############
    #direnv
    #fuse
    #nix-index
    #smartmontools
    wl-clipboard
    #nix-init
    #bottom # System monitoring
    #nixpacks
    libqalculate
    wget
  ];
  nixpkgs.overlays = [
    (self: super: {
      mpv = super.mpv.override {
        scripts = [ self.mpvScripts.mpris ];
      };
    })
  ];
}

