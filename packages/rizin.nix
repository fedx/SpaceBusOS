{config, pkgs, inputs, ...}:

#let
#  pkgs.cutterPlugins.rz-retdec = pkgs.mkDerivation rec{
#    src = inputs.retdec;
#    buildInputs = [ pkgs.rizin pkgs.retdec ];
#  };
#in
{
  environment.systemPackages = with pkgs; [
    #(cutter.withPlugins (ps: with ps; [
    #  jsdec 
    #  rz-ghidra 
    #  sigdb
    #]))
    (rizin.withPlugins (ps: with ps; [
      jsdec 
      rz-ghidra 
      sigdb
    ]))
  ];
}
