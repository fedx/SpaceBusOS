{ config, pkgs, ...}:
let
  sbos_src = pkgs.runCommand "sbos_src" {} "cp -r $(pwd) $out";
in
{
  environment.systemPackages = [
    sbos_src
  ];
  # Yeah, I know that's not the most elegant solution, but it works...
  system.activationScripts.sbos_config.text = ''
    ln -nsf ${sbos_src}/ /home/fedx/SpaceBusOS
  '';
}

