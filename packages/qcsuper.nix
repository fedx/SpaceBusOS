{ lib
, python3
, makeWrapper
, fetchFromGitHub
}:

#  pycrate = python3.pkgs.buildPythonApplication rec {
#    pname = "pycrate";
#    version = "0.7.2";
#    pyporject = true;
#
#    src = fetchFromGitHub {
#      owner = "pycrate-org";
#      repo = "pycrate";
#      rev = version;
#      hash = "sha256-NLLj7JdqT8gQ1Ic9X3huH170T1cnG6VkGmWTQAdRB+A=";
#    };
#    nativeBuildInputs = [ makeWrapper ];
#    propagatedBuildInputs = with python3.pkgs; [
#      importlib-resources
#    ];
#    pythonImportsCheck = ["pycrate"];
#  };
let
pycrate = python3.pkgs.buildPythonApplication rec {
  pname = "pycrate";
  version = "0.7.2";
  pyproject = true;

  src = fetchFromGitHub {
    owner = "pycrate-org";
    repo = "pycrate";
    rev = version;
    hash = "sha256-NLLj7JdqT8gQ1Ic9X3huH170T1cnG6VkGmWTQAdRB+A=";
  };

  nativeBuildInputs = [
    python3.pkgs.setuptools
    python3.pkgs.wheel
  ];

  #pythonImportsCheck = [ "pycrate" ];

  meta = with lib; {
    description = "A Python library to ease the development of encoders and decoders for various protocols and file formats, especially telecom ones. Provides an ASN.1 compiler and a CSN.1 runtime";
    homepage = "https://github.com/pycrate-org/pycrate";
    license = licenses.lgpl21Only;
    maintainers = with maintainers; [ ];
    mainProgram = "pycrate";
  };
  };
in

python3.pkgs.buildPythonApplication rec {
  pname = "qc-super";
  version = "2.0.1";
  pyproject = true;

  src = fetchFromGitHub {
    owner = "P1sec";
    repo = "QCSuper";
    rev = version;
    hash = "sha256-m75yoFO+NR5WyckmJfPtjXAajbDHB2PFBc7sznVQnw8=";
  };
  nativeBuildInputs = [ makeWrapper ];
    
  propagatedBuildInputs = with python3.pkgs; [
    pyserial
    setuptools
    crcmod
    pyusb
    (pycrate)
  ];
  #pythonImportsCheck = [ "qc_super" ];

  meta = with lib; {
    description = "QCSuper is a tool communicating with Qualcomm-based phones and modems, allowing to capture raw 2G/3G/4G radio frames, among other things";
    homepage = "https://github.com/P1sec/QCSuper";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ ];
    mainProgram = "qc-super";
  };
}
