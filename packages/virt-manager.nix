{ config, pkgs, ... }:
{
  virtualisation = {
    lxc.enable = true;
    lxd.enable = true;
    libvirtd.enable = true;
    spiceUSBRedirection.enable = true;
  };
  security.wrappers.spice-client-glib-usb-acl-helper.source = "${pkgs.spice-gtk}/bin/spice-client-glib-usb-acl-helper";
  programs.virt-manager.enable = true;
  users.groups.libvirtd.members = ["fedx"];
  environment.systemPackages = with pkgs; [
    virt-manager
    spice-gtk
    virt-viewer
    #distrobuilder
    #cdrkit
    #hivex
    #wimlib
    qemu-utils
    qemu_full
    qemu_kvm
    virtio-win
    swtpm
  ];
}
