{config, pkgs, ...}:
{
  environment.systemPackages = with pkgs; [
    (gnuradio.override {
      extraPackages = with gnuradioPackages; [
        osmosdr
      ];
    })
  ];
}

