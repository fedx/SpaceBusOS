# Specifically to prevent Ghdra from accessing the internet.
{ pkgs, config, lib, ... }:
let
  librewolf-script = pkgs.writeShellScriptBin "thunderbird" ''
    ${pkgs.bubblewrap}/bin/bwrap \
      --unshare-all \
      --hostname THUNDERBIRD \
      --share-net \
      --ro-bind / / \
      --dev /dev \
      --proc /proc \
      --tmpfs /home \
      --dir /home/fedx \
      --bind /home/fedx/.thunderbird /home/fedx/.thunderbird \
      --bind /run/user/1000/dconf/user /run/user/1000/dconf/user \
      --bind /home/fedx/.config /home/fedx/.config \
      --bind /home/fedx/.local/thunderbird /home/fedx/thunderbird \
      ${pkgs.thunderbird-wayland}/bin/thunderbird "$@"
  '';
  #This makes sure that the script replaces the default symlink in librewolf.
  thunderbird-low =
    pkgs.thunderbird-wayland.overrideAttrs (oldAttrs: { meta.priority = 10; });
in {
  environment.systemPackages =
    [ thunderbird-low pkgs.bubblewrap librewolf-script ];
}
