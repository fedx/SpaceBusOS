{ config, pkgs, lib, ... }:
let 
  version = "2.6.0-alpha6";
in
{
   # [...]
   nixpkgs.overlays = [ (self: super: 
{
  prusaslicer = super.prusa-slicer.overrideAttrs (old: {
    version = "${version}";
    
    src = super.fetchurl {
      url = "https://github.com/prusa3d/PrusaSlicer/archive/refs/tags/version_${version}.tar.gz";
      sha256 = "sha256-ctCLDcUL9v6+aj/YaSALxrqvp+ky87nuG6hkFabpilg=";
    };
  buildInputs = with pkgs; [
    binutils
    boost
    cereal
    cgal_5
    curl
    dbus
    eigen
    expat
    glew
    glib
    gmp
    gtk3
    hicolor-icon-theme
    ilmbase
    libpng
    mpfr
    nlopt
    opencascade-occt
    openvdb
    pcre
    tbb
    # wxGTK-prusa
    xorg.libX11
    qhull
  ];
    patches = [];
 #      patches = [
    # # Fix detection of TBB, see https://github.com/prusa3d/PrusaSlicer/issues/6355
    # (super.fetchpatch {
    #   url = "https://github.com/prusa3d/PrusaSlicer/commit/76f4d6fa98bda633694b30a6e16d58665a634680.patch";
    #   sha256 = "1r806ycp704ckwzgrw1940hh1l6fpz0k1ww3p37jdk6mygv53nv6";
    # })
    # (super.fetchpatch {
    #   url = "https://github.com/prusa3d/PrusaSlicer/commit/926ae0471800abd1e5335e251a5934570eb8f6ff.patch";
    #   sha256 = "sha256-tAEgubeGGKFWY7r7p/6pmI2HXUGKi2TM1X5ILVZVT20=";
    # })
  # ];

  });
}


) ];
  environment.systemPackages = with pkgs; [ prusaslicer ];
}

