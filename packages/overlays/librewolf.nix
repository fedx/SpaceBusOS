# Specifically to prevent Ghdra from accessing the internet.
{ pkgs, config, lib, ... }:
let
  librewolf-script = pkgs.writeShellScriptBin "librewolf" ''
    ${pkgs.bubblewrap}/bin/bwrap \
      --unshare-all \
      --hostname LIBREWOLF \
      --share-net \
      --ro-bind / / \
      --dev /dev \
      --proc /proc \
      --tmpfs /home \
      --dir /home/fedx \
      --bind /home/fedx/.librewolf /home/fedx/.librewolf \
      --bind /run/user/1000/dconf/user /run/user/1000/dconf/user \
      --bind /home/fedx/.config /home/fedx/.config \
      --bind /home/fedx/.local/librewolf /home/fedx/Downloads \
      ${pkgs.librewolf-wayland}/bin/librewolf "$@"
  '';
  #This makes sure that the script replaces the default symlink in librewolf.
  librewolf-low =
    pkgs.librewolf-wayland.overrideAttrs (oldAttrs: { meta.priority = 10; });
in {
  environment.systemPackages =
    [ librewolf-low pkgs.bubblewrap librewolf-script ];
}
