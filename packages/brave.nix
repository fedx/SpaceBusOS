{ pkgs, config, lib, ... }:
  let
    braveScript = pkgs.writeShellScriptBin "brave"
     ''
       printf "Running Brave in sandboxed mode. \n"
       ${lib.getBin pkgs.bubblewrap}/bin/bwrap \
         --ro-bind /nix /nix \
         --ro-bind /run/current-system/sw/bin /run/current-system/sw/bin \
         --unshare-all \
         --share-net \
         --hostname REDACTED \
         --dev /dev \
         --dev-bind /dev/dri /dev/dri \
         --proc /proc \
         --ro-bind /etc /etc \
         --ro-bind /sys/dev/char /sys/dev/char \
         --ro-bind /sys/devices /sys/devices \
         --ro-bind /run/dbus /run/dbus \
         --dir "/run/user/$(${lib.getBin pkgs.coreutils-full}/bin/id -u)" \
         --ro-bind "/run/user/$(id -u)/wayland-0" "/run/user/$(${lib.getBin pkgs.coreutils-full}/bin/id -u)/wayland-0" \
         --ro-bind "/run/user/$(id -u)/pipewire-0" "/run/user/$(${lib.getBin pkgs.coreutils-full}/bin/id -u)/pipewire-0" \
         --ro-bind "/run/user/$(id -u)/pulse" "/run/user/$(${lib.getBin pkgs.coreutils-full}/bin/id -u)/pulse" \
         --tmpfs /tmp \
         --tmpfs /home \
         --dir $HOME \
         --bind $HOME/.cache $HOME/.cache \
         --bind $HOME/.local/share/applications/ $HOME/.local/share/applications/ \
         --bind $HOME/.config/BraveSoftware/ $HOME/.config/BraveSoftware/ \
         --bind $HOME/Downloads/ $HOME/Downloads \
         ${lib.getBin pkgs.brave}/bin/brave  --enable-features=UseOzonePlatform --ozone-platform=wayland
     '';
     lowBrave = pkgs.brave.overrideAttrs (oldAttrs: { meta.priority = 10; });

 in

# Ok, so unfortunatly it does not look like Bubblewrap works the way I was wanting it to.
# It's a very nice application, but the big thing I am looking for is explicit blacklisting.
# For not I am going to keep it because other than that it's amazing, and I don't know what else to use...
 {
   environment.systemPackages = [
     braveScript
     lowBrave
     pkgs.bubblewrap
     pkgs.libfido2
   ];
 }

