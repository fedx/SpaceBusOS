{ config, pkgs, ...}:{
  programs.neovim = {
    enable = true;
    defaultEditor=true;
    viAlias = true;
    vimAlias = true;
    configure = {
      customRC = ''
        set number
        let g:rainbow_active = 1
      '';
      packages.myVimPackage = with pkgs.vimPlugins; {
        start = [ 
          fugitive 
	  rainbow
	  #openscad-nvim
	  #dracula-nvim
	  yuck-vim
	  vCoolor-vim
	  vim-css-color
        ];
      };
    };

  };
}
