#{ config, lib, pkgs, ...}:
#{
#  programs.steam = {
#    enable = true;
#    remotePlay.openFirewall = true;
#  };
#  environment.systemPackages = [
#    pkgs.proton-caller
#  ];
#  programs.firejail.wrappedBinaries = {
#    steam = {
#      executable = "${lib.getBin pkgs.steam}/bin/steam";
#      profile = "${pkgs.firejail}/etc/firejail/steam.profile";
#  };
#    steam-run = {
#      executable = "${lib.getBin pkgs.steam}/bin/steam-run";
#      profile = "${pkgs.firejail}/etc/firejail/steam-run.profile";
#    };
#    proton-caller = {
#      executable = "${lib.getBin pkgs.proton-caller}/bin/proton-caller";
#      profile = "${pkgs.firejail}/etc/firejail/proton-caller.profile";
#    };
#
#
#  };
#}

#^^^ Old version of config. Firejail does not work in this case because... reasons. ^^^

{ pkgs, config, lib, ... }:
# let
#   steamScript = pkgs.writeShellScriptBin "steam"
#     ''
#       ${lib.getBin pkgs.bubblewrap}/bin/bwrap \
#         --ro-bind ${lib.getBin pkgs.steam}/bin/* /usr/bin \
#           --setenv HOME /home/example \
#           --setenv GTK_THEME Adwaita:dark \
#           --setenv MOZ_ENABLE_WAYLAND 1 \
#           --setenv PATH /usr/bin \
#           --hostname RESTRICTED \
#           --unshare-all \
#           --share-net \
#           --die-with-parent \
#           --new-session \
#           ${lib.getBin pkgs.steam}/bin/steam
#     '';
#   lowSteam = pkgs.steam.overrideAttrs (oldAttrs: { meta.priority = 10; });
# in
# Unfortunatly steam does not like being sandboxed with FireJial, so we are now using Bubblewrap.
# Welp. It turns out that Steam is already sandboxed in Bubblewrap by default. Woops.
{
  programs.steam.enable = true;
  environment.systemPackages = [
    pkgs.mangohud
    pkgs.proton-caller
  ];
}
