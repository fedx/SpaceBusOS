{ config, pkgs, lib, ... }:
let
  commit = "457391b0380335d5e9a5babdec90ac53928b23b4";
  diffHash = "sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=";
  shorthash = lib.strings.substring 0 7 commit;
  kernelVersion = "6.3.0";
in {
  boot.kernelPackages = pkgs.linuxPackagesFor (pkgs.linux_latest.override {
    argsOverride = rec {
      src = pkgs.fetchurl {
        url = "mirror://kernel/linux/kernel/v6.x/linux-6.3.tar.xz";
        hash = "sha256-ujSR9e1r0nCjcMRAQ049aQhfzdUoki+gHnPXZX23Ox4=";
      };
      version = "${kernelVersion}";
      modDirVersion = "${kernelVersion}";
      };
  });
  boot.kernelParams = [ 
    "security=selinux" 
    "scheduler=bfq" 
    "iommu=pt" 
    "intel_iommu=on" 
    "zswap.enabled=1"
    "zswap.compressor=lz4"
    "zswap.zpool=z3fold"
  ];
  boot.initrd.kernelModules = [
    "lz4"
    "z3fold"
    "xhci_pci" 
    "nvme" 
    "rtsx_pci_sdmmc"  
  ];
  boot.kernelPatches = [
    {
      name = "bcachefs-${commit}";
      patch = pkgs.fetchurl {
        name = "bcachefs-${commit}.diff";
        url =
          "https://evilpiepirate.org/git/bcachefs.git/rawdiff/?id=${commit}&id2=v${lib.versions.majorMinor kernelVersion}";
        sha256 = diffHash;
      };
    }
    {
      name = "SBOS-config";
      patch = null;
      structuredConfig = with lib.kernel; {
        ignoreConfigErrors = true;
        #####################
        # ZSWAP/ZRAM/ZCache #
        #####################
        ZSWAP = yes;
        HAVE_KERNEL_LZO = yes;
        LZO_COMPRESS = yes;
        LZO_DECOMPRESS = yes; 
        DECOMPRESS_LZO = yes; 
        CRYPTO_LZO = yes;
        ZIMAGE = yes; 
        CLEANCACHE = yes; 
        ####################
        # Scheduler Tuning #
        ####################
        IOSCHED_BFQ = yes;
        MQ_IOSCHED_DEADLINE = no;
        #####################
        # Filesystem Config #
        #####################
        BCACHEFS_FS = yes;
        BCACHEFS_POSIX_ACL = yes;
        BCACHEFS_QUOTA = yes;
        ANDROID_BINDERFS = yes; # For waydroid support
        F2FS = no;
        NTFS_FS = no;
        ADFS_FS = no;
        AFFS_FS = no;
        BFS_FS = no;
        BEFS_FS = no;
        CRAMFS = no;
        EROFS_FS = no;
        VXFS_FS = no;
        HFS_FS = no;
        HPFS_FS = no;
        JFS_FS = no;
        MINIX_FS = no;
        NILFS2_FS = no;
        OMFS_FS = no;
        QNX4FS_FS = no;
        QNX6FS_FS = no;
        SYSV_FS = no;
        UFS_FS = no;
        EXT2_FS = no;
        EXT3_FS = no;
        XFS_FS = no;
        OCFS2_FS = no;
        AFS_FS = no;
        UBIFS_FS = no;
        JFFS2_FS = no;
        HFSPLUS_FS = no;
        ECRYPT_FS = no;
        AFF_FS = no;
        CEF_FS = no;
        CRYPTO_CRC32C_INTEL = yes;
        FUSE_FS = yes;
        ################################
        # General Security Improvments #
        ################################
        PAGE_POISONING = yes;
        PANIC_TIMEOUT = freeform "-1";
        SECURITY_SAFESETID = yes;
        ##################################
        ## General Liquorix/Zen configs ##
        ##################################
        GCC_PLUGINS = yes;
        GCC_PLUGIN_LATENT_ENTROPY = yes;
        GCC_PLUGIN_STRUCTLEAK_BYREF_ALL = yes;
        # CONFIG_STACK_VALIDATION gives better stack traces. Also is enabled in all official kernel packages by Archlinux team
        CONFIG_STACK_VALIDATION = yes;
        # Enable IKCONFIG following Arch's philosoph
        CONFIG_IKCONFIG = yes;
        CONFIG_IKCONFIG_PROC = yes;
        # Compress modules by default (following Arch's kernel)
        CONFIG_MODULE_COMPRESS_ZSTD = yes;
        #Enable FUNCTION_TRACER/GRAPH_TRACER"
        CONFIG_FUNCTION_TRACER = yes;
        CONFIG_STACK_TRACER = yes;
        #####################################################################
        ## Removing all unnessisaary parts of the kernel. USE WITH CAUTION ##
        #####################################################################

        CONFIG_CPU_SUP_AMD = no;
        CONFIG_AMD_IOMMU = no;
        CONFIG_X86_MCE_AM = no;
        CONFIG_PERF_EVENTS_AMD_POWER = no;
        CONFIG_PERF_EVENTS_AMD_UNCOR = no;

      };
    }
  ];
  environment.systemPackages = with pkgs; [ policycoreutils bcachefs-tools];
  systemd.package = pkgs.systemd.override { withSelinux = true; };
}
