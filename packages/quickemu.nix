{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    quickemu
  ];
  virtualisation.libvirtd = {
    enable = true;
    allowedBridges = [
      "virbr0"
      "br0"
    ];
  };
}
