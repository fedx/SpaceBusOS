{config, pkgs, inputs, ...}:{

  environment.systemPackages = with pkgs; [
    (libsigrok.overrideAttrs{
      src = inputs.libsigrok;
    } )
    pulseview
    sigrok-cli
  ];
  services.udev = {
    packages = [pkgs.libsigrok ];
    extraRules = ''
      ATTRS{idVendor}=="8102", ATTRS{idProduct}=="8102", ENV{ID_SIGROK}="1"
    '';
  };
}
