{ config, pkgs, ... }: 
{
#pkgs.wrapOBS.override { obs-studio = pkgs.obs-studio; } {
#  plugins = with pkgs.obs-studio-plugins; [
#    wlrobs
#  ];
environment.systemPackages = [ pkgs.obs-studio ];
boot.extraModulePackages = with config.boot.kernelPackages; [
    v4l2loopback
  ];
}
