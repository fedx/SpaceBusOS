hardware.firmware = [
  (pkgs.stdenvNoCC.mkDerivation {
    name = "brcm-firmware";

    buildCommand = ''
      dir="$out/lib/firmware"
      mkdir -p "$dir"
      cp -r ${./files/firmware}/* "$dir"
    '';
  })
];
