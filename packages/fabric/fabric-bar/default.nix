{ 
  lib, 
  python3Packages, 
  gtk3, 
  gtk-layer-shell, 
  cairo, 
  gobject-introspection, 
  libdbusmenu-gtk3, 
  gdk-pixbuf, 
  wrapGAppsHook3, 
  ...
}:

python3Packages.buildPythonApplication {
  pname = "bar";
  version = "0.0.1";
  pyproject = true;

  src = ./.;

  nativeBuildInputs = [
    wrapGAppsHook3
    gtk3
    gobject-introspection
    cairo
  ];
  buildInputs = [
    libdbusmenu-gtk3
    gtk-layer-shell
    gdk-pixbuf
  ];
  dependencies = with python3Packages; [ python-fabric ];
  doCheck = false;
  dontWrapGApps = true;

  preFixup = ''
    makeWrapperArgs+=("''${gappsWrapperArgs[@]}")
  '';

}
