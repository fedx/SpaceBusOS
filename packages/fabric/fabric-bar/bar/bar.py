import psutil
import subprocess
from fabric import Application
from fabric.widgets.box import Box
from fabric.widgets.label import Label
from fabric.widgets.overlay import Overlay
from fabric.widgets.eventbox import EventBox
from fabric.widgets.datetime import DateTime
from fabric.widgets.centerbox import CenterBox
from fabric.system_tray.widgets import SystemTray
from fabric.widgets.circularprogressbar import CircularProgressBar
from fabric.widgets.wayland import WaylandWindow as Window
from fabric.hyprland.widgets import Language, ActiveWindow, Workspaces, WorkspaceButton
from fabric.utils import (
    FormattedString,
    bulk_replace,
    invoke_repeater,
    get_relative_path,
)

size=18
class StatusBar(Window):
    def __init__(
        self,
    ):
        super().__init__(
            name="bar",
            layer="top",
            anchor="left top right",
            margin="0px 0px 1px 0px",
            exclusivity="auto",
            visible=False,
            all_visible=False,
        )
        self.workspaces = Workspaces(
            name="workspaces",
            spacing=4,
            buttons_factory=lambda ws_id: WorkspaceButton(id=ws_id, label=None),
        )
        self.active_window = ActiveWindow(name="hyprland-window")
        self.date_time = DateTime(name="date-time", formatters="%Y-%m-%d %H:%M:%S")

        self.ram_progress_bar = CircularProgressBar(
            name="ram-progress-bar", pie=False, size=size, line_width=2
        )
        self.cpu_progress_bar = CircularProgressBar(
            name="cpu-progress-bar", pie=False, size=size, line_width = 2
        )
        self.bat_progress_bar = CircularProgressBar(
            name="bat-progress-bar", pie=False, size=size, line_width = 2
        )
        self.vol_progress_bar = CircularProgressBar(
            name="vol-progress-bar", pie=False, size=size, line_width = 2
        )
        self.cpu_overlay = Overlay(
            child=self.cpu_progress_bar,
            overlays=[
                Label("󰻠", style="margin: 0px 0px 0px 0px; font-size: 12px"),
            ],
        )
        self.ram_overlay = Overlay(
            child=self.ram_progress_bar,
            overlays=[
                Label("󰍛", style="margin: 0px 0px 0px 0px; font-size: 12px"),
            ],
        )
        self.bat_overlay = Overlay(
          child=self.bat_progress_bar,
          overlays=[
              Label("󰁼",  style="margin: 0px 0px 0px 0px; font-size: 12px"), 
          ],
        )
        self.vol_overlay = Overlay(
          child=self.vol_progress_bar,
          overlays=[
              Label("󰕾",  style="margin: 0px 0px 0px 0px; font-size: 12px"), 
          ],
        )
        self.status_container = Box(
            name="widgets-container",
            spacing=4,
            orientation="h",
            children=[
                self.vol_overlay,
                self.cpu_overlay,
                self.ram_overlay,
                self.bat_overlay
            ],
        )
        #self.status_container.add(VolumeWidget()) if AUDIO_WIDGET is True else None

        self.children = CenterBox(
            name="bar-inner",
            start_children=Box(
                name="start-container",
                spacing=4,
                orientation="h",
                children=self.workspaces,
            ),
            center_children=Box(
                name="center-container",
                spacing=4,
                orientation="h",
                children=self.active_window,
            ),
            end_children=Box(
                name="end-container",
                spacing=4,
                orientation="h",
                children=[
                    self.status_container,
                    #self.system_tray,
                    self.date_time,
                ],
            ),
        )

        invoke_repeater(1000, self.update_progress_bars)

        self.show_all()

    def update_progress_bars(self):
        self.ram_progress_bar.value = psutil.virtual_memory().percent / 100
        self.cpu_progress_bar.value = psutil.cpu_percent() / 100
        self.bat_progress_bar.value = psutil.sensors_battery().percent / 100
        self.vol_progress_bar.value = float(subprocess.run(["wpctl", "get-volume", "@DEFAULT_AUDIO_SINK@"],  capture_output=True).stdout[-5:-1])
        return True





if __name__ == "__main__":
    main()

def main():
    bar = StatusBar()
    app = Application("bar", bar)
    app.set_stylesheet_from_file(get_relative_path("./bar.css"))

    app.run()


