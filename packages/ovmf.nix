{ config, pkgs, ... }:
{
  nixpkgs = {
    overlays = [
      (final: prev: {
        OVMF = prev.OVMFFull.override { secureBoot = true; };
      })
    ];
  };
  environment.systemPackages = with pkgs; [
    OVMF
  ];
}
