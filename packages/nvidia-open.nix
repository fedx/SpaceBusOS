{ config, pkgs, ... }: 
let
  nvidia-offload = pkgs.writeTextFile {
    name = "prime-offload";
    destination = "/bin/prime-offload";
    executable = true;
    text = ''
      #!${pkgs.bash}/bin/bash
      export __NV_PRIME_RENDER_OFFLOAD=1
      export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
      export __GLX_VENDOR_LIBRARY_NAME=nvidia 
      export __VK_LAYER_NV_optimus=NVIDIA_only
      exec "$@"
    '';
  };

in
{
  boot.kernelParams = [ "i915.force_probe=9a49" ];
  #boot.initrd.availableKernelModules = [ "nvidia" "nvidia_modeset" "nvidia_uvm" "nvidia_drm" ];

  nixpkgs.config.allowUnfree = true;
  ##############################################
  ## NVIDIA Drivers
  ##############################################
  services = {
    xserver = {
      enable = true;
      videoDrivers = [ "modesetting" "nvidia" ];
      screenSection = ''
        Option         "metamodes" "nvidia-auto-select +0+0 {ForceFullCompositionPipeline=On}"
        Option         "AllowIndirectGLXProtocol" "off"
        Option         "TripleBuffer" "on"
      '';
    };
  };
  hardware = {
    nvidia = {
      #enable = false;
      #package = config.boot.kernelPackages.nvidia_x11_beta;
      package = config.boot.kernelPackages.nvidiaPackages.stable;
      #open = true;
      powerManagement = {
        enable = true;
        finegrained = true;
      };
      modesetting.enable = true;
      prime = {
        offload.enable = true;
        nvidiaBusId = "PCI:23:00:0";
        intelBusId = "PCI:00:02:0";
      };
    };
    opengl = {
      enable = true; 
      extraPackages = with pkgs; [
        intel-media-driver
	intel-vaapi-driver
	vaapiVdpau
	libvdpau-va-gl
      ];
    };
  };
  environment.sessionVariables = { LIBVA_DRIVER_NAME = "iHD"; };
  #environment.systemPackages = with pkgs; [
  #  intel-media-driver
  #  vaapiIntel #vaapiVdpau
    #nvtop-nvidia
    #nvidia-offload
    #nvtop
    #nvidia-vaapi-driver
    #vaapi-intel-hybrid
    #libdpau-va-gl000
  #];
  #virtualisation.podman.enableNvidia = true;
}
