{config, pkgs, ...}:
{
  programs.weylus = {
    enable = true;
    users = ["fedx"];
  }
}
