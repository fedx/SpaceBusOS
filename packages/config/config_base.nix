{ config, pkgs, ...}:
let
  sbos_config = pkgs.runCommand "configs" {} "cp -r ${../../config/base} $out";
in
{
  environment.systemPackages = [
    sbos_config
    pkgs.exa
    pkgs.starship
    pkgs.mcfly
    pkgs.bat
    pkgs.duf
    pkgs.zellij
  ];
  # Yeah, I know that's not the most elegant solution, but it works...
  system.activationScripts.sbos_config.text = ''
    ln -nsf ${sbos_config}/bashrc /home/fedx/.bashrc
    ln -nsf ${sbos_config}/config.fish /home/fedx/.config/fish/config.fish
    ln -nsf ${sbos_config}/gitconfig /home/fedx/.config/git/config
    ln -nsf ${sbos_config}/starship.toml /home/fedx/.config/starship.toml
  '';
}

