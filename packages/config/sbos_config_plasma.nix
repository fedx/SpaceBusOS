{ config, pkgs, ...}:
let
sbos_config_plasma = pkgs.runCommand "configs" {} "cp -r ${../../config/plasma} $out";
in
{
  # Yeah, I know that's not the most elegant solution, but it works...
  system.activationScripts.sbos_config.text = ''
    ln -nsf ${sbos_config_plasma}/plasmashellrc /home/fedx/.config/plasmashellrc
    ln -nsf ${sbos_config_plasma}/konsolerc /home/fedx/.config/konsolerc
    ln -nsf ${sbos_config_plasma}/plasmarc /home/fedx/.config/plasmarc
  '';
}

