{ pkgs, config, lib, ... }:
let
  version = "1.0.1";
in
{
  nixpkgs = {
    overlays = [
      (self: super: {
        system76-firmware = super.system76-firmware.overrideAttrs (oldAttrs: rec {
          src = super.fetchFromGitHub {
            owner = "pop-os";
            repo = "system76-firmware";
            rev = "1.0.39";
            sha256 = "sha256-nPHBL73hmvW9z5SQjmfu+ozMXxUEajNQxNtE/V9QwZ0=";
          };
          cargoDeps = oldAttrs.cargoDeps.overrideAttrs (lib.const {
            name = "1.0.39.tar.gz";
            inherit src;
            outputHash = "sha256-a2spYRDKzn4dLWpvhYdZcFQEOvniE0k0RFPzcMgny3Q=";
          });
        });
#         "system76-io-module-${version}-${pkgs.kernel.version}" = super."system76-io-module-${version}-${pkgs.kernel.version}".overrideAttrs (old: {
#           patches = (old.patches or []) ++ [
#             ./io_fix.diff
#           ];
#           });
      })
    ];
  };
}
