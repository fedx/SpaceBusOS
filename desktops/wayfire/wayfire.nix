{ config, pkgs, inputs, ... }:


let
  dbus-wayfire-environment = pkgs.writeTextFile {
    name = "dbus-wayfire-environment";
    destination = "/bin/dbus-wayfire-environment";
    executable = true;

    text = ''
      dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=hypr
      systemctl --user stop pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
      systemctl --user start pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
    '';
  };

  configure-gtk = pkgs.writeTextFile {
    name = "configure-gtk";
    destination = "/bin/configure-gtk";
    executable = true;
    text = let
      schema = pkgs.gsettings-desktop-schemas;
      datadir = "${schema}/share/gsettings-schemas/${schema.name}";
    in ''
      export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
      gnome_schema=org.gnome.desktop.interface
      gsettings set $gnome_schema gtk-theme 'Dracula'
      gsettings set $gnome_schema gtk-icons 'Candy'
    '';
  };
  #hypr_config = pkgs.runCommand "configs" {} "cp -r ${./config} $out";
  
in {
  imports = [ 
    ../../services/pipewire.nix
  ];
  

programs.wayfire = {
  enable = true;
  plugins = with pkgs.wayfirePlugins; [
    wcm
    wf-shell
    wayfire-plugins-extra
  ];
};

  fonts = {
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
      liberation_ttf
      fira-code
      fira-code-symbols
      mplus-outline-fonts.githubRelease
      dina-font
      proggyfonts
      jetbrains-mono
    ];
    fontconfig.defaultFonts = {
      serif = [ "jetbrains-mono" ];
      sansSerif = [ "jetbrains-mono" ];
    };
  };

  programs.dconf.enable = true;

  environment.systemPackages = with pkgs; [
    #hyprland
    #hypr_config
    wofi
    #tofi
    alacritty
    kitty
    #termpdfpy
    themechanger
    gtk3-x11
    libsForQt5.qt5ct
    configure-gtk
    xdg-utils # for openning default programms when clicking links
    glib # gsettings
    dracula-theme # gtk theme
    #adwaita-icon-theme # default gnome cursors
    grim # screenshot functionality
    slurp # screenshot functionality
    wl-clipboard # wl-copy and wl-paste for copy/paste from stdin / stdout
    #hyprpaper
    #waybar
    eww
    qt6.qtwayland
    libsForQt5.qt5.qtwayland
    wlsunset
    pavucontrol
    #hyprlock
    #hypridle
    #hyprpicker
    libnotify
    mako
  ];
  
  environment.variables = {
    XDG_SESSION_TYPE = "wayland";
    QT_QPA_PLATFORMTHEME = "qt5ct";
    SDL_VIDEODRIVER="wayland";
    MOZ_ENABLE_WAYLAND="1";
    GTK_THEME="Dracula";
  };
  #system.activationScripts.sbos_config.text = ''
  #  ln -nsf ${hypr_config}/hyprland.conf /home/fedx/.config/hypr/hyprland.conf
  #  ln -nsf ${hypr_config}/hyprpaper.conf /home/fedx/.config/hypr/hyprpaper.conf
  #  ln -nsf ${hypr_config}/hyprlock.conf /home/fedx/.config/hypr/hyprlock.conf
  #  ln -nsf ${hypr_config}/hypridle.conf /home/fedx/.config/hypr/hypridle.conf
  #  ln -nsf ${hypr_config}/eww/eww.yuck /home/fedx/.config/eww/eww.yuck
  #  ln -nsf ${hypr_config}/eww/eww.scss /home/fedx/.config/eww/eww.scss
  #  ln -nsf ${hypr_config}/mako /home/fedx/.config/mako/config
  #'';
}

