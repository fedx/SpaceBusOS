{config, pkgs, ...}:let 
  dbus-river-environment = pkgs.writeTextFile {
    name = "dbus-river-environment";
    destination = "/bin/dbus-river-environment";
    executable = true;

    text = ''
      dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
      systemctl --user stop pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
      systemctl --user start pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
    '';
  };

  # currently, there is some friction between sway and gtk:
  # https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland
  # the suggested way to set gtk settings is with gsettings
  # for gsettings to work, we need to tell it where the schemas are
  # using the XDG_DATA_DIR environment variable
  # run at the end of sway config
  configure-gtk = pkgs.writeTextFile {
    name = "configure-gtk";
    destination = "/bin/configure-gtk";
    executable = true;
    text = let
      schema = pkgs.gsettings-desktop-schemas;
      datadir = "${schema}/share/gsettings-schemas/${schema.name}";
    in ''
      export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
      gnome_schema=org.gnome.desktop.interface
      gsettings set $gnome_schema gtk-theme 'Dracula'
    '';
  };  


  river_config = pkgs.runCommand "configs" {} "cp -r ${./config} $out";



in {
  imports = [
    ../../services/pipewire.nix
  ];
  programs.river.enable = true; 
  environment.systemPackages = with pkgs; [
    dbus-river-environment
    configure-gtk
    wlr-randr
    waybar
    alacritty
    wofi
    lisgd
    themechanger
    gtk3-x11
    libsForQt5.qt5ct
    configure-gtk
    xdg-utils # for openning default programms when clicking links
    glib # gsettings
    dracula-theme # gtk theme
    adwaita-icon-theme # default gnome cursors
    grim # screenshot functionality
    slurp # screenshot functionality
    wl-clipboard # wl-copy and wl-paste for copy/paste from stdin / stdout
    qt6.qtwayland
    libsForQt5.qt5.qtwayland
    wlsunset
    pavucontrol
    libnotify
    mako
    kitty
  ];
    xdg.portal = {
      enable = true;
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };
  system.activationScripts.sbos_config.text = ''
    ln -nsf ${river_config}/init /home/fedx/.config/river/init
  '';

  fonts = {
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
      liberation_ttf
      fira-code
      fira-code-symbols
      mplus-outline-fonts.githubRelease
      dina-font
      proggyfonts
      jetbrains-mono
    ];
    fontconfig.defaultFonts = {
      serif = [ "jetbrains-mono" ];
      sansSerif = [ "jetbrains-mono" ];
    };
  };

  environment.variables = {
    XDG_SESSION_TYPE = "wayland";
    QT_QPA_PLATFORMTHEME = "qt5ct";
    SDL_VIDEODRIVER="wayland";
    MOZ_ENABLE_WAYLAND="1";
    GTK_THEME="Dracula";
  };
  programs.dconf.enable = true;
  systemd.user.services.xdg-desktop-portal-gtk = {
    wantedBy = [ "xdg-desktop-portal.service" ];
    before = [ "xdg-desktop-portal.service" ];
  };
}
