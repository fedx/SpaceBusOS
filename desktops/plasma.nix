{ config, pkgs, ... }: {
  imports = [ ../services/pipewire.nix ];
  services = {
    xserver = {
      enable = true;
      displayManager.sddm.enable = true;
      desktopManager.plasma5.enable = true;
      xkb.layout = "us";
    };
  };
  #   nixpkgs.overlays = [
  #     (self: super: {
  #       bismuth = super.libsForQt5.bismuth.overrideAttrs
  #         (old: rec { version = "3.1.4"; });
  #     })
  #   ];
  services.libinput.enable = true;
  programs.xwayland.enable = true;
  environment.systemPackages = with pkgs; [
    libsForQt5.ark
    #libsForQt5.bismuth
    libsForQt5.bismuth
    libsForQt5.kdeconnect-kde
    libsForQt5.konsole
    libsForQt5.kate
    libsForQt5.sonnet
    libsForQt5.plasma-systemmonitor
    libsForQt5.kmail
    libsForQt5.akonadi
    #libsForQt5.krfb
    libsForQt5.krdc
    libsForQt5.khotkeys
    libsForQt5.yakuake
    libsForQt5.elisa
    libsForQt5.gwenview
    libsForQt5.okular
    libsForQt5.oxygen
    libsForQt5.khelpcenter
    libsForQt5.konsole
    libsForQt5.plasma-browser-integration
    libsForQt5.print-manager
    aspell
    aspellDicts.en
    aspellDicts.en-science
    aspellDicts.en-computers

  ];
  programs.dconf.enable = true;
}
