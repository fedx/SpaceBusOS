{ config, pkgs, ... }: {
  services = {
    xserver = {
      enable = true;
      displayManager.sddm.enable = true;
      #desktopManager.plasma5.enable = true;
      layout = "us";
      libinput.enable = true;
    };
  };
  environment.systemPackages = with pkgs; [ wayfire ];
}
