{ config, pkgs, ... }:
let
  dbus-hypr-environment = pkgs.writeTextFile {
    name = "dbus-hypr-environment";
    destination = "/bin/dbus-hypr-environment";
    executable = true;

    text = ''
      dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=hypr
      systemctl --user stop pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
      systemctl --user start pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
    '';
  };

  configure-gtk = pkgs.writeTextFile {
    name = "configure-gtk";
    destination = "/bin/configure-gtk";
    executable = true;
    text = let
      schema = pkgs.gsettings-desktop-schemas;
      datadir = "${schema}/share/gsettings-schemas/${schema.name}";
    in ''
      export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
      gnome_schema=org.gnome.desktop.interface
      gsettings set $gnome_schema gtk-theme 'Dracula'
      gsettings set $gnome_schema gtk-icons 'Candy'
    '';
  };

in {
  imports = [ ../packages/candy-icons.nix ../services/pipewire.nix ];
  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
    nvidiaPatches = false;

  };

  fonts = {
    fonts = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      font-awesome
      source-han-sans
      source-han-sans-japanese
      source-han-serif-japanese
      jetbrains-mono
    ];
    fontconfig.defaultFonts = {
      serif = [ "jetbrains-mono" ];
      sansSerif = [ "jetbrains-mono" ];
    };
  };

  programs.dconf.enable = true;
  # ##############################################
  # boot.kernelParams = [
  #   "i915.fastboot=1"
  #   "i915.modeset=1"
  # ];
  services = {
    xserver = {
     enable = true;
      wacom.enable = true;
  #     videoDrivers = [ "modesetting" "nouveau" "i915" ];
  #     screenSection = ''
  #       Option         "AllowIndirectGLXProtocol" "off"
  #       Option         "TripleBuffer" "on"
  #       Option          "DRI"  "iris"
  #     '';
  #   };
  };
  # hardware = {
  #   opengl.enable = true; # Needed to get CUDA working.
  };



  xdg.portal = {
    enable = true;
    wlr.enable = true;
    # gtk portal needed to make gtk apps happy
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };
  environment.systemPackages = with pkgs; [
    wofi
    alacritty
    kitty
    themechanger
    # dolphin
    gtk3-x11
    libsForQt5.qt5ct
    configure-gtk
    xdg-utils # for openning default programms when clicking links
    glib # gsettings
    dracula-theme # gtk theme
    gnome3.adwaita-icon-theme # default gnome cursors
    grim # screenshot functionality
    # slurp # screenshot functionality
    wl-clipboard # wl-copy and wl-paste for copy/paste from stdin / stdout
    libsForQt5.kwallet
    hyprpaper
    waybar
  ];
  environment.variables = {
    # LIBVA_DRIVER_NAME = "nvidia";
    XDG_SESSION_TYPE = "wayland";
    # GBM_BACKEND = "nvidia-drm";
    # __GLX_VENDOR_LIBRARY_NAME = "nvidia";
    # WLR_NO_HARDWARE_CURSORS = "1";
    QT_QPA_PLATFORMTHEME = "qt5ct";
    SDL_VIDEODRIVER="wayland";
    MOZ_ENABLE_WAYLAND="1";
    GTK_THEME="Dracula";
  };
  # systemd.sleep.extraConfig = "AllowSusspend=no";
}
