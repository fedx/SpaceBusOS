{config, pkgs, hyprgrass, inputs, ...}:{
  environment.systemPackages = [
    #pkgs.stable.hyprlandPlugins.hyprexpo
    #pkgs.stable.hyprlandPlugins.hy3
    #inputs.rose-pine-hyprcursor.packages.${pkgs.system}.default
    #inputs.hyprgrass.packages.${pkgs.system}.default
    inputs.hy3.packages.${pkgs.system}.default
  ];
}
