{ lib, pkgs, modulesPath, ... }: {
  #imports = [
  #  self.nixosModules.default

  #  "${builtins.toString modulesPath}/virtualisation/qemu-vm.nix"
  #];

  #services.desktopManager.cosmic.enable = true;
  #services.displayManager.cosmic-greeter.enable = true;

  #services.flatpak.enable = true;
  #services.gnome.gnome-keyring.enable = true;

  environment.systemPackages = with pkgs;
    [
      #(pkgs.callPackage ./chronos.nix {})
      cosmic-bg
      cosmic-osd
      cosmic-term
      cosmic-edit
      cosmic-comp
      cosmic-panel
      cosmic-icons
      cosmic-files
      cosmic-session
      cosmic-greeter
      cosmic-applets
      cosmic-settings
      cosmic-launcher
      cosmic-protocols
      cosmic-screenshot
      cosmic-applibrary
      cosmic-design-demo
      cosmic-notifications
      cosmic-settings-daemon
      cosmic-workspaces-epoch
    ];

  environment.sessionVariables = { COSMIC_DATA_CONTROL_ENABLED = "1"; };


}
