# Desktops available.

Add a desktop by calling `./desktops/<desktop>.nix` in the imports section of your conigs. In the future, this will also install pre-defined configs for the desktop, but that's still a todo.

## Suported Desktops:

- [X] Plasma (configs in `/config/plasma/`, but untested and only there for future development.
- [ ] GNOME
- [X] Sway (NOT TESTED!)
- [ ] XFCE
- [ ] Wayfire
- [ ] i3wm
- [ ] bspwm
- [ ] herbstluftwm
- [ ] Tilix
- [ ] XMonad
- [ ] LXQT (potentialy with a KWin backend)
- [ ] Qtile
- [ ] Mate
- [ ] Cinnamon

... and more ...
