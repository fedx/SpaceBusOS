{ config, pkgs, ... }:
{
  imports = [
    ../packages/sbos_config_plasma.nix
    ../services/pipewire.nix
  ];
  services = {
    xserver = {
      enable = true;
      displayManager.sddm.enable = true;
      desktopManager.gnome.enable = true;
      layout = "us";
      libinput.enable = true;

  };
  power-profiles-daemon.enable = false;
  };
  environment.systemPackages = with pkgs; [
    gnome.gnome-tweaks
    gnome.gnome-shell-extensions
    gnomeExtensions.paperwm
  ];
}
