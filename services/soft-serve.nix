{config, pkgs, ...}:
{
  systemd.services.softserve = {
      serviceConfig = {
        ExecStart = "${pkgs.soft-serve}/bin/soft serve";
        Restart=on-failure;
        RestartSec=10;
        KillMode=process;
        RuntimeDirectory = "Soft-Serve";
        RootDirectory = "/var/Soft-Serve";
        BindReadOnlyPaths = [
          "/nix/store"

          # So tmux uses /bin/sh as shell
        ];
        PrivateDevices = true;
      };
    };
    environment.systemPackages = with pkgs; [ soft-serve ];
}
