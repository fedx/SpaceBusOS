{config, pkgs, ...}:
{
  services = {
    cachix-deploy = {
      enable = true; 

    };
    cachix-agent = {
      enable = true; 
    };
  };
}
