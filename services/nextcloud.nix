{ config, pkgs, ...}:
#/etc/nixos/configuration.nix
{
  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud24;
    hostName = "SpaceBusOS";
#     https = true;

    config = {
      adminpassFile = "/etc/nixos/passwd.txt";
      extraTrustedDomains = [ "100.96.62.101" "0.0.0.0" "100.107.206.75" ];
#       overwriteProtocol = "https";
    };
  };
    networking.firewall.allowedTCPPorts = [80 443];
#     services.nginx = {
#       recommendedGzipSettings = true;
#       recommendedOptimisation = true;
#       recommendedProxySettings = true;
#       recommendedTlsSettings = true;
#       sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";
#       commonHttpConfig = ''
#       # Add HSTS header with preloading to HTTPS requests.
#       # Adding this header to HTTP requests is discouraged
#       map $scheme $hsts_header {
#           https   "max-age=31536000; includeSubdomains; preload";
#       }
#       add_header Strict-Transport-Security $hsts_header;
#
#       # Enable CSP for your services.
#       #add_header Content-Security-Policy "script-src 'self'; object-src 'none'; base-uri 'none';" always;
#
#       # Minimize information leaked to other domains
#       add_header 'Referrer-Policy' 'origin-when-cross-origin';
#
#       # Disable embedding as a frame
#       add_header X-Frame-Options DENY;
#
#       # Prevent injection of code in other mime types (XSS Attacks)
#       add_header X-Content-Type-Options nosniff;
#
#       # Enable XSS protection of the browser.
#       # May be unnecessary when CSP is configured properly (see above)
#       add_header X-XSS-Protection "1; mode=block";
#
#       # This might create errors
#       proxy_cookie_path / "/; secure; HttpOnly; SameSite=strict";
#     '';
#     };
}
