{config, pkgs, ...}:{
  services.mpd = {
    enable = true;
    musicDirectory = "/home/fedx/Music";
    extraConfig = ''
      audio_output {
        type "pipewire"
        name "PipeWire Out"
      }
    '';
    # Optional:
    #network.listenAddress = "any"; # if you want to allow non-localhost connections
    #network.startWhenNeeded = true; # systemd feature: only start MPD service upon connection to its socket
  };
  environment.systemPackages = with pkgs; [
    
  ];
}
