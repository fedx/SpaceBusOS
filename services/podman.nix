{ pkgs, config, ... }: {
  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
  };
  environment.systemPackages = with pkgs; [ pkgs.podman-compose ];
  virtualisation.oci-containers.backend = "podman";
}
