{ config, pkgs, ... }:
{
  virtualisation.waydroid.enable = false;
  virtualisation.lxd.enable = false;
  virtualisation.lxc.enable = false;
  environment.systemPackages = [
    pkgs.waydroid
  ];
}
