{ config, pkgs, sbospkgs, ... }: {
  systemd.packages = with sbospkgs; [ cockpit ];
  system.activationScripts = {
    cockpit = ''
      mkdir -p /etc/cockpit/ws-certs.d
      chmod 755 /etc/cockpit/ws-certs.d
      cp ${conf} /etc/cockpit/cockpit.conf
    '';
  };

  security.pam.services.cockpit = { };
  environment.systemPackages = with sbospkgs; [
    cockpit
    cockpit-machines
    libvirt-dbus
  ];
  environment.pathsToLink = [ "/share/cockpit" ];

  systemd.sockets.cockpit.listenStreams = [ 9090 ];
}
