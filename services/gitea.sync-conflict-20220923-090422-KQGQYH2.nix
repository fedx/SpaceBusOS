{ pkgs, config, ...}:

{
  services.gitea = {
    enable = true;
    domain = "100.107.206.75";
  };
  networking.firewall.allowedTCPPorts = [3000];
}
