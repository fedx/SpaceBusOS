{config, pkgs, ...}:
{
  services.invidious = {
    enable = true;
    settings = {
      quality = "dash"; 
      quality_dash = "best";
    };
  };
}
