{ lib, pkgs, config, ... }: {
  systemd.timers."sbci" = {
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnBootSec = "5m";
      OnUnitActiveSec = "5m";
      Unit = "hello-world.service";
    };
  };

  systemd.services."sbci_build" = {
    script = ''
      if [ "$(http https://github.com/Porchetta-Industries/CrackMapExec/commits/master.atom | rg "<updated>" | head --lines 1 | cut -c 12-31)" != "$(cat "/home/fedx/SBCI/content/builds/$(exa --sort=created /home/fedx/SBCI/content/builds/ | rg CME | tail -n 1)" | head -n 5 | rg date | cut -c 10-30)" ]; then
      	outfile=/home/fedx/SBCI/content/builds/CME-build"$(date -u +'%Y-%m-%d::%H:%M')".md
      	printf '+++' >>"${outfile}"
      	printf "title = CME Build $(date) \n" >>"${outfile}"
      	printf 'author = "SpaceBot" \n ' >>"${outfile}"
      	printf "date =  $(http https://github.com/Porchetta-Industries/CrackMapExec/commits/master.atom | rg "<updated>" | head --lines 1 | cut -c 12-31)\n" >>"${outfile}"
      	printf 'tags = ["builds"] \n' >>"${outfile}"
      	printf '+++\n' >>"${outfile}"
      	printf '```\n' >>"${outfile}"
      	nix build github:Porchetta-Industries/CrackMapExec >>"${outfile}"
      	printf '```' >>"${outfile}"
      else
      	printf "Nothing to do."
      fi
    '';
    serviceConfig = {
      Type = "oneshot";
      User = "fedx";
    };
  };
  systemd.services."sbci_server" = {
    wantedBy = [ "multi-user.target" ];
    serviceConfig.ExecStart = "${pkgs.hugo}/bin/hugo serve /home/fedx/SBCI";
  };
}
