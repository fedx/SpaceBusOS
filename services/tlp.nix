{config, pkpgs, ...}:
{
  services.tlp = {
    enable = true;
    settings = {
      TLP_ENABLE="1";
      TLP_DEFAULT_MODE="BAT";
      TLP_PERSISTENT_DEFAULT="0";
      SOUND_POWER_SAVE_ON_BAT="1";
      SOUND_POWER_SAVE_ON_AC="0";
      SOUND_POWER_SAVE_CONTROLLER="Y";
    };
  };

}
