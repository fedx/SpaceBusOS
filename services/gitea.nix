{ pkgs, config, ... }:

{
  services.gitea = {
    enable = true;
    domain = "resolute.hyrax-dragon.ts.net";
    rootUrl = "resolute.hyrax-dragon.ts.net";
    lfs.enable = true;

  };
}
