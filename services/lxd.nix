{ config, pkgs, ... }:
{
  virtualisation = {
    #lxc.enable = true;
    lxd.enable = true;
  };
  environment.systemPackages = with pkgs; [
    distrobuilder
    cdrkit
    hivex
    wimlib
    qemu-utils
    qemu_full
    qemu_kvm
  ];
}
