{ config, pkgs, ... }:
#/etc/nixos/configuration.nix
{
  services.adguardhome = {
    enable = true;
    settings."tls" = {
      "enabled" = true;
      "force_https" = true;
      "server_name" = "ghost.hyrax-dragon.ts.net";
      "port_https" = 443;
    };
  };
}
