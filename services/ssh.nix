{ config, pkgs, ... }: {
  services.openssh = {
    enable = true;
    settings.X11Forwarding = true;
  };
  programs.ssh = {
    forwardX11 = true;
  };
}
