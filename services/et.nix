{ config, pkgs, ... }: {
  services.eternal-terminal.enable = true;
  environment.systemPackages = [ pkgs.eternal-terminal pkgs.tmux pkgs.zellij ];

}
