{ config, lib, pkgs, ...}:
#/etc/nixos/configuration.nix

{
  services.searx = {
    enable = true;
    settings = {
      package = pkgs.searxng;
      server.port = 8080;
      server.bind_address = "0.0.0.0";
      server.secret_key = "saved-undead-retold";
      # engines = lib.singleton{
      #   brave = {
      #     name = "brave";
      #     shortcut = "br";
      #     engine = "brave";
      #     enable = true;
      #   };
      #     ygtorrent = {
      #       name = "yggtorrent";
      #       enable = false;
      #   };
      # };
    };

  };
}
