{config, pkgs, ...}:
{
  services.oomd = {
    enable = true;
    enableUserServices = true;
    extraConfig = {
      DefaultMemoryPressureDurationSec = "20s"; 
    };
  };
}
