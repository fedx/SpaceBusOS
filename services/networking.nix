{ config, pkgs, ... }: {
  networking = {
    useDHCP = false;
    wireless.enable = false;
    firewall = {
      enable = true;
      trustedInterfaces = [ "tailscale0" ];
      checkReversePath = "loose";
    };

    networkmanager = {
      enable = true;
      #dns = "none";

      wifi = {
        scanRandMacAddress = true;
        macAddress = "random";
      };
      ethernet = { macAddress = "random"; };

    };
  };
}
