{ config, pkgs, ... }: {
  virtualisation.oci-containers.containers = {
    woodpecker-server = {
      image = "woodpeckerci/woodpecker-server:latest";
      ports = [ "8000:8000" ];
      volumes = [ "woodpecker-server-data:/var/lib/woodpecker/" ];
      environment = {
        WOODPECKER_OPEN = "true";
        WOODPECKER_HOST = "http://localhost:8000";
        WOODPECKER_GITHUB_CLIENT = "WOODPECKER_GITHUB_CLIENT";
        WOODPECKER_GITHUB_SECRET = "WOODPECKER_GITHUB_SECRET";
        WOODPECKER_AGENT_SECRET = "SUPER_SECRET_KEY!!!";
      };
    };
    woodpecker-agent = { image = "woodpeckerci/woodpecker-agent:latest"; };
  };
}
