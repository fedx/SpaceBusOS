{ config, pkgs, ...}:
{
  services.samba = {
    enable = true; 
    openFirewall = true;
    shares.public = {
      path = "/home/fedx/public";
      browseable = "yes";
      "guest ok" = "yes";
      "read only" = false;
      comment = "Public smaba share.";
    };
  }
}
