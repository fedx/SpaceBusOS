{ config, pkgs, ... }: {
  services.nextdns.enable = true;
  environment.systemPackages = with pkgs; [ nextdns ];
}
