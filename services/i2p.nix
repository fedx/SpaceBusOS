{config, pkgs, ...}:

{
  services.i2pd = {
    enable = true;
  };
  environment.systemPackages = with pkgs; [ xd ];
}
