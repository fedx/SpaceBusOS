{ config, pkgs, ... }: {
  services.syncthing = {
    enable = true;
    group = "syncTheThings";
    user = "fedx";
    dataDir = "/home/fedx";    # Default folder for new synced folders
    configDir = "/home/myusername/Documents/.config/syncthing";

  };
}
