{ config, pkgs, ... }:
{
  services.opensnitch = {
    enable = true;
  };
  environment.systemPackages = [ pkgs.opensnitch-ui ];
}

