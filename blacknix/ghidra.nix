# Specifically to prevent Ghdra from accessing the internet.
{ pkgs, config, lib, ... }:
let
  ghidra-script = pkgs.writeShellScriptBin "ghidra" ''
    ${pkgs.bubblewrap}/bin/bwrap \
      --unshare-all \
      --ro-bind / / \
      --dev /dev \
      --proc /proc \
      --tmpfs /home \
      --dir /home/fedx \
      --bind /home/fedx/.ghidra /home/fedx/.ghidra \
      --bind /home/fedx/.local/ghidra /home/fedx/ghidra \
      --bind /home/fedx/.java /home/fedx/.java \
      --bind /home/fedx/.cache/JNA /home/fedx/.cache/JNA
      ${pkgs.ghidra}/bin/ghidra "$@"
  '';
  #This makes sure that the script replaces the default symlink in ghidra.
  ghidra-low = pkgs.ghidra.overrideAttrs (oldAttrs: { meta.priority = 10; });
in {
  environment.systemPackages = [ ghidra-low pkgs.bubblewrap ghidra-script ];
}
