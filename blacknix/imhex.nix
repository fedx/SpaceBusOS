# Specifically to prevent Ghdra from accessing the internet.
{ pkgs, config, lib, ... }:
let
  imhex-script = pkgs.writeShellScriptBin "imhex" ''
    ${pkgs.bubblewrap}/bin/bwrap \
      --unshare-all \
      --ro-bind / / \
      --dev /dev \
      --proc /proc \
      --tmpfs /home \
      --dir /home/fedx \
      --bind /home/fedx/.config/imhex /home/fedx/.config/imhex \
      ${pkgs.ghidra}/bin/ghidra "$@"
  '';
  #This makes sure that the script replaces the default symlink in ghidra.
  ghidra-low = pkgs.ghidra.overrideAttrs (oldAttrs: { meta.priority = 10; });
in {
  environment.systemPackages = [ ghidra-low pkgs.bubblewrap ghidra-script ];
}
