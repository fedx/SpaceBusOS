{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.11";  
    flake-utils.url = "github:numtide/flake-utils";
    libsigrok = {
      url = "github:sigrokproject/libsigrok";
      flake = false;
    };
    QCSuper = {
      url = "github:P1sec/QCSuper";
      flake = false;
    };
    fabric.url = "github:Fabric-Development/fabric";

    #blender_2_4_npr{
    #  url = "https://cdn.builder.blender.org/download/experimental/blender-4.4.0-alpha+npr-prototype.fa2bb3267c56-linux.x86_64-release.tar.xz"
    #  flake = false;
    #};
    #blender_2_4_alpha{
    #  url="https://cdn.builder.blender.org/download/daily/blender-4.4.0-alpha+main.95be928c5ef2-linux.x86_64-release.tar.xz";
    #  flake = false;
    #};
    hyprland.url = "github:hyprwm/Hyprland?ref=v0.46.2";
    #hyprnotify = {
    #  url = "github:codelif/hyprnotify";
    #  flake = false;
    #};
    hyprland-plugins = {
      url = "github:hyprwm/hyprland-plugins";
      inputs.hyprland.follows = "hyprland";
    };
    hy3 = {
      url = "github:outfoxxed/hy3?ref=hl0.46.0";
      inputs.hyprland.follows = "hyprland";
    };
    #hyprgrass = {
    #  url = "github:horriblename/hyprgrass";
    #  inputs.hyprland.follows = "hyprland"; # IMPORTANT
    #};
    #rose-pine-hyprcursor.url = "github:ndom91/rose-pine-hyprcursor";
    #};
    nixos-cosmic = { 
      url = "github:lilyinstarlight/nixos-cosmic";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, nixpkgs-stable, nixos-hardware, fabric, nixos-cosmic, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { };
      nix-overlay = final: prev: {
        stable = nixpkgs-stable.legacyPackages.${prev.system};
#	stab-blender = nixpkgs-blender.legacyPackages.${prev.system};
      };

    in {
      nixosConfigurations.RazorCrest = inputs.nixpkgs.lib.nixosSystem {
        system="x86_64-linux";
	specialArgs = { inherit inputs; };	
	modules = [
          ./configuration.nix
          ./bootloaders/systemd.nix
          ./users/users.nix
          #./desktops/hyprland/hypr.nix
	  #./desktops/river/river.nix
          #./services/podman.nix
	  ./services/ssh.nix
	  #./packages/wireshark.nix
	  ./packages/desktop.nix
	  ./services/tailscale.nix
          ./services/networking.nix
	  ./services/tlp.nix
	  ./packages/rizin.nix
	  #./packages/sigrok.nix
	  ./services/fwupd.nix
	  ./services/invidious.nix
	  ./services/searx.nix
	  ./hardware/hackrf.nix
	  ./hardware/rtl-sdr.nix
	  ./services/playerctl.nix
	  #./packages/fabric/default.nix
	  ./packages/virt-manager.nix
	  #./services/syncthing.nix
	  #./desktops/wayfire/wayfire.nix
	  ./desktops/sway/sway.nix
	   #nixos-cosmic.nixosModules.default
	  nixos-hardware.nixosModules.framework-16-7040-amd
	({ config, pkgs, inputs, ... }: { 
          nixpkgs.overlays = [ nix-overlay inputs.fabric.overlays.${system}.default ];
	  networking.hostName = "RazorCrest";
	  boot.kernelPackages = pkgs.linuxPackages_zen;
	  boot.kernel.sysctl = { "net.ipv4.tcp_syncookies" = false; "vm.swappiness" = 10; };
	  boot.initrd.kernelModules = [ "amdgpu" "mt7921e" ];
	  hardware.enableRedistributableFirmware = true;
	  services.xserver.enable = true;
	  services.xserver.videoDrivers = [ "amdgpu" ];
	  services.fprintd.enable = false;
	  services.power-profiles-daemon.enable = false;
	  hardware.graphics.enable = true;
	  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
	  hardware.graphics.extraPackages = with pkgs; [
	    rocmPackages.clr.icd
	    amdvlk
	    qmk_hid
            qmk
	  ];
            fileSystems."/" = {
	      device = "/dev/nvme0n1p1";
              fsType = "bcachefs";
              options = [ "noatime" "defaults" "compression=zstd" "background_compression=zstd"];
            };

            fileSystems."/boot" = {
              device = "/dev/nvme0n1p2";
              fsType = "vfat";
	   };
	   services.udev.extraRules = ''
	     KERNEL=="hidraw*", MODE="0660", GROUP="lexa", TAG+="uaccess", TAG+="udev-acl"
	   '';
               #nix.settings = {
               #    substituters = [ "https://cosmic.cachix.org/" ];
               #    trusted-public-keys = [ "cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE=" ];
                # };
		# services.desktopManager.cosmic.enable = true;
		# services.displayManager.cosmic-greeter.enable = true;
              

	})


	];

      };
      nixosConfigurations.Gauntlet = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
	specialArgs = { inherit inputs; };
	modules = [
          ./configuration.nix
	  ./bootloaders/systemd.nix
	  ./users/users.nix
	  ./desktops/hyprland/hypr.nix
	  ./packages/desktop.nix
	  ./services/ssh.nix
	  ./services/networking.nix
	  ./services/tailscale.nix
	  ./services/fwupd.nix
          ./packages/fabric/default.nix
	  #nixos-hardware.starlabs-starlite-mkv
	  ({ config, pkgs, lib, ...}: {
  	    nixpkgs.overlays = [ nix-overlay inputs.fabric.overlays.${system}.default  ];
	    hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
	    services.xserver.enable = true;
	    services.xserver.videoDrivers = [  ];
	    hardware.graphics.enable = true;
	    systemd.tpm2.enable = false;
	    systemd.units."dev-tpmrm0.device".enable = false;
	    hardware.graphics.extraPackages = with pkgs; [
	      rocmPackages.clr.icd
	      amdvlk
	    ];
	    boot.kernelPackages = pkgs.linuxPackages_zen;
	    networking.hostName = "Gauntlet";
              fileSystems."/" ={ 
	        device = "/dev/disk/by-uuid/f6aaf75c-8efd-4f4d-8054-4b48e9768a4e";
                fsType = "ext4";
	       };
	       fileSystems."/boot" ={ 
	         device = "/dev/disk/by-uuid/A4D4-5B97";
		 fsType = "vfat";
		 options = [ "fmask=0022" "dmask=0022" ];
	       };
	  })
	];
      };
      nixosConfigurations.SBOSGalp5 = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { inherit inputs; };
        modules = [
          ./configuration.nix
          ./bootloaders/systemd.nix
          ./users/users.nix
          ./desktops/hypr.nix
          ./services/podman.nix
	  ./packages/desktop.nix
	  ./services/tailscale.nix
          ./services/networking.nix
          ./services/ssh.nix
	  ./packages/nvidia-open.nix
	  ./services/tlp.nix
	  ./packages/rizin.nix
	  ./packages/sigrok.nix
	  nixos-hardware.nixosModules.system76
          ({ pkgs, ... }: {

            # boot.kernelPackages = pkgs.linuxPackages_zen;
	    # boot.kernelPackages = pkgs.linuxPackages_lqx;
            boot.kernelPackages = pkgs.linuxPackages_zen;
            boot.kernelParams = [ "zswap=on" "iommu=pt" "intel_iommu=on" "zswap.enabled=1" "zswap.compressor=lz4" "zswap.zpool=z3fold"];
            boot.initrd.kernelModules = [ "lz4" "z3fold" "xhci_pci" "nvme" "rtsx_pci_sdmmc"  ];
            ###########
            # Flatpak #
            ###########
	    services.flatpak.enable = true;
            services.printing.enable = true; 

	    networking.hostName = "SBOSGalp5";

            nixpkgs.config.allowUnfree = true;
            ##############################################
            ## Drive config
            ##############################################

             
            boot.kernelModules = [ "kvm-intel" "iwlwifi" "iwlmvm" ];
            boot.extraModulePackages = [ ];
            hardware.enableRedistributableFirmware = true;

	    
            fileSystems."/" = {
	      device = "/dev/nvme0n1p1";
              fsType = "bcachefs";
              options = [ "noatime" "defaults" "compression=zstd" "background_compression=zstd"];
            };

            fileSystems."/boot" = {
              device = "/dev/disk/by-uuid/44A7-C31C";
              fsType = "vfat";
            };

            ##############################################
            ## System Pacakges
            ##############################################
           
            environment.systemPackages = with pkgs; [
	      ##logrotate
              zig
              intel-media-driver
	      vaapiIntel
              fuse
              nix-index
              smartmontools
              wl-clipboard
              nix-init
              # nvtop-nvidia # Nvidia monitoring
              bottom # System monitoring
              nixpacks
              libqalculate
              wget
	     
            ];
            ##############################################
            ## System 76 drivers
            ##############################################
            hardware.system76 = {
              enableAll = true;
              power-daemon.enable = true;
              firmware-daemon.enable = true;
            };
	    #hardware.opengl.extraPackages = with pkgs; [
		#intel-media-driver
		#intel-vaapi-driver
	    #];
            hardware.rtl-sdr.enable = true;
            hardware.hackrf.enable = true;
	    programs.adb.enable = true;
            #programs.adb.enable = true;
            # This value determines the NixOS release from which the defaultO
            # settings for stateful data, like file locations and database versions
            # on your system were taken. It‘s perfectly fine and recommended to leave
            # this value at the release version of the first install of this system.
            # Before changing this value read the documentation for this option
            # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
            system.stateVersion = "23.11"; # Did you read the comment?
          })

        ];
      };
      nixosConfigurations.SBOS_Serv2 = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { inherit inputs; };
        modules = [
          #inputs.sbos-package.nixosModules.cockpit
          ./configuration.nix
          ./bootloaders/systemd.nix
          ./users/users.nix
          ./packages/virt-manager.nix
          ./packages/quickemu.nix
          ./services/podman.nix
          ./services/nextcloud.nix
          ./services/gitea.nix
          ./services/hedgedoc.nix
          ./services/syncthing.nix
          ./services/et.nix
          ./services/hydra.nix
          ./services/tailscale.nix
          #./services/soft-serve.nix
          ({ pkgs, ... }: {
            ##############################################
            ## Drive config
            ##############################################
            #This is migrated from hardware-configuration.nix.
            #This may contain reduntent or arbitrary code.

            #         imports =
            #         [ (modulesPath + "/installer/scan/not-detected.nix")
            #         ];

            boot.initrd.availableKernelModules =
              [ "xhci_pci" "ahci" "usb_storage" "usbhid" "sd_mod" "sr_mod" ];
            boot.initrd.kernelModules = [ ];
            boot.kernelModules = [ "kvm-intel" ];
            boot.extraModulePackages = [ ];

            fileSystems."/" = {
              device = "/dev/disk/by-uuid/7ceaa81c-13d1-4ad5-bad6-32e42082653f";
              fsType = "btrfs";
              options = [ "ssd" "noatime" "compress=zstd" "space_cache" ];

            };
            fileSystems."/boot" = {
              device = "/dev/disk/by-uuid/C12D-0E4B";
              fsType = "vfat";
            };

            fileSystems."/home/fedx/mnt/drive-1" = {
              device = "/dev/disk/by-uuid/bf9eea01-fa28-4fd3-ad8f-a02bf9b352ab";
              fsType = "btrfs";
              options = [ "noatime" "compress=zstd:15" "space_cache" ];
            };

            fileSystems."/home/fedx/mnt/drive-2" = {
              device = "/dev/disk/by-uuid/631e9931-fd73-48d1-baa3-13d01d19c2be";
              fsType = "btrfs";
              options = [ "noatime" "compress=zstd:15" "space_cache" ];
            };
            fileSystems."/home/fedx/.vault" = {
              device = "/dev/disk/by-uuid/d7025520-99bb-47df-b251-77ff5eb17dec";
              fsType = "btrfs";
              options = [ "noatime" "compress=zstd:15" "space_cache" ];
            };
            #fileSystems."/home/fedx/.vault" = {
            #  device = "/dev/disk/by-uuid/28699c8d-0982-43b0-944c-e054f283a780";
            #  fsType = "btrfs";
            #  options = [ "noatime" "compress=zstd:15" "space_cache" ];
            #};
            swapDevices = [ ];
            #fileSystems."/var/lib/nextcloud" = {
            #  device = "/dev/sdc1";
            #  fsType = "btrfs";
            #  options = [ "defaults" "compress=zstd" "noatime" "nodiratime" ];
            #};
            #powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
            systemd.units."sys-subsystem-net-devices-tun0.device".enable =
              false;
            systemd.units."sys-subsystem-net-devices-waydroid0.device".enable =
              false;
            systemd.units."sys-subsystem-net-devices-vethQVZnUw.device".enable =
              false;
            networking.hostName = "SBOS_Serv2";
            environment.systemPackages = with pkgs; [
              fish
              fuse
              nix-index
              git-annex
              parted
              aspell
              aspellDicts.en-computers
              aspellDicts.en-science
              aspellDicts.en
              hunspell
            ];
            # This value determines the NixOS release from which the defaultO
            # settings for stateful data, like file locations and database versions
            # on your system were taken. It‘s perfectly fine and recommended to leave
            # this value at the release version of the first install of this system.
            # Before changing this value read the documentation for this option
            # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
            system.stateVersion = "23.05"; # Did you read the comment?
          })

        ];
      };
      nixosConfigurations.ghost = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { inherit inputs; };
        modules = [
          ./configuration.nix
          ./packages/linux_sbos.nix
          ./bootloaders/systemd.nix
          ./users/users.nix
          ./services/podman.nix
          ./services/et.nix
          ./services/networking.nix
          ./services/tailscale.nix
          ./services/adguard.nix
          ./services/searx.nix
          ({ pkgs, ... }: {
            boot.initrd.availableKernelModules =
              [ "xhci_pci" "ahci" "usb_storage" "usbhid" "sd_mod" "sr_mod" ];
            boot.initrd.kernelModules = [ ];
            boot.kernelModules = [ "kvm-intel" ];
            boot.extraModulePackages = [ ];

            fileSystems."/" = {
              device = "/dev/disk/by-uuid/a145cc69-397b-465d-9d2e-4260844c564c";
              fsType = "btrfs";
              options = [ "defaults" "noatime" "compress=zstd" "ssd" ];

            };
            fileSystems."/boot" = {
              device = "/dev/disk/by-uuid/315C-B9B8";
              fsType = "vfat";
            };
            swapDevices = [{
              device = "/dev/disk/by-uuid/eb475b14-bb58-4f3d-bb3e-b98e3fcd59a3";
            }];
            networking.hostName = "ghost";
            environment.systemPackages = with pkgs; [
              fish
              fuse
              nix-index
              git-annex
              parted
            ];
            services.sshd.enable = true;
            # This value determines the NixOS release from which the defaultO
            # settings for stateful data, like file locations and database versions
            # on your system were taken. It‘s perfectly fine and recommended to leave
            # this value at the release version of the first install of this system.
            # Before changing this value read the documentation for this option
            # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
            system.stateVersion = "23.05"; # Did you read the comment?
          })

        ];
      };
     nixosConfigurations.lamda = inputs.nixpkgs.lib.nixosSystem {
        system = "aarch64-linux";
        specialArgs = { inherit inputs; };
        modules = [
          ./configuration.nix
          ./bootloaders/systemd.nix
          ./users/users.nix
          ./services/podman.nix
          ./services/et.nix
          ./services/networking.nix
          ./services/tailscale.nix
          ./desktops/hypr-mobile.nix
          ({ pkgs, ... }: {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = false;
            home-manager.users.fedx = import ./homemng/lambda.nix;
            boot.initrd.kernelModules = [ ];
            boot.extraModulePackages = [ ];

            #fileSystems."/" = {
            #  device = "/dev/disk/by-uuid/a145cc69-397b-465d-9d2e-4260844c564c";
            #  fsType = "btrfs";
             # options = [ "defaults" "noatime" "compress=zstd" "ssd" ];

            #};
            #fileSystems."/boot" = {
            #  device = "/dev/disk/by-uuid/315C-B9B8";
            #  fsType = "vfat";
            #};
            #swapDevices = [{
             # device = "/dev/disk/by-uuid/eb475b14-bb58-4f3d-bb3e-b98e3fcd59a3";
           # }];
           # mobile-nixos.boot.stage-1.networking.enable = true;
            networking.hostName = "lamda";
            environment.systemPackages = with pkgs; [
              fish
              fuse
              nix-index
              git-annex
              parted
            ];
            # This value determines the NixOS release from which the defaultO
            # settings for stateful data, like file locations and database versions
            # on your system were taken. It‘s perfectly fine and recommended to leave
            # this value at the release version of the first install of this system.
            # Before changing this value read the documentation for this option
            # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
            system.stateVersion = "23.05"; # Did you read the comment?
          })

        ];
      };
      nixosConfigurations.hmp1 = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { inherit inputs; };
        modules = [
          ./configuration.nix
          ./bootloaders/systemd.nix
	  ./users/users.nix
          ./services/networking.nix
          ./desktops/hypr.nix
	  ./services/tailscale.nix
	  ./packages/desktop.nix
	  ./services/tlp.nix
	  #./packages/sbos.nix
	 ./services/podman.nix
	  nixos-hardware.nixosModules.apple-macbook-air-6

          ({ config, pkgs, lib, ... }: {
	    nixpkgs.config.allowUnfree = true;
	    nixpkgs.config.allowBroken = true;
            boot.initrd.kernelModules = [ "mt7921e" "lz4" "z3fold" "xhci_pci" "nvme" "rtsx_pci_sdmmc" "wl"];
	    boot.kernelParams = [ "zswap=on" "iommu=pt" "intel_iommu=on" "zswap.enabled=1" "zswap.compressor=lz4" "zswap.zpool=z3fold"];
	    boot.extraModulePackages = with config.boot.kernelPackages; [ broadcom_sta ];
	    boot.supportedFilesystems = [ "bcachefs" ];
            networking.hostName = "hmp1";
            environment.systemPackages = with pkgs; [
	      intel-media-driver
              bcachefs-tools
	    ];
	    fileSystems."/" = {
              device = "/dev/disk/by-uuid/6661c47a-8666-4848-a16b-103085f66df5";
              fsType = "bcachefs";
              options = [ "noatime" "defaults" "compression=zstd" "background_compression=zstd"];
	    };
	    fileSystems."/boot" = {
              device = "/dev/disk/by-uuid/3818-5731";
              fsType = "vfat";
            };
	    swapDevices = [{
              device = "/dev/disk/by-uuid/25211d9c-8635-4ff3-8283-9d88e08939fa";
            }];
            # This value determines the NixOS release from which the defaultO
            # settings for stateful data, like file locations and database versions
            # on your system were taken. It‘s perfectly fine and recommended to leave
            # this value at the release version of the first install of this system.
            # Before changing this value read the documentation for this option
            # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
            system.stateVersion = "23.10"; # Did you read the comment?
          })

        ];
      };
      nixosConfigurations.mbp = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
	specialArgs = { inherit inputs; };
	modules = [
          ./configuration.nix
	  ./bootloaders/systemd.nix
	  ./users/users.nix
	  ./services/networking.nix
	  ./desktops/hypr.nix
	  ./services/tailscale.nix
	  ./packages/desktop.nix
	  ./services/tlp.nix
	  ./packages/zsh.nix
	  ./packages/brcm_fw.nix
	  nixos-hardware.nixosModules.apple-t2
	  ({config, pkgs, ...}:{
            boot.initrd.kernelModules = [ "mt7921e" "lz4" "z3fold" "xhci_pci" "nvme" "rtsx_pci_sdmmc" ];
            boot.kernelParams = [ "zswap=on" "iommu=pt" "intel_iommu=on" "zswap.enabled=1" "zswap.compressor=lz4" "zswap.zpool=z3fold"];
            boot.extraModulePackages = with config.boot.kernelPackages; [ ];
            boot.supportedFilesystems = [ "bcachefs" ];
            networking.hostName = "mbp";
            environment.systemPackages = with pkgs; [
              intel-media-driver
              bcachefs-tools
            ];
	    services.mbpfan = {
              enable = true;
	      aggressive = true; 
	    };
            # This value determines the NixOS release from which the defaultO
            # settings for stateful data, like file locations and database versions
            # on your system were taken. It‘s perfectly fine and recommended to leave
            # this value at the release version of the first install of this system.
            # Before changing this value read the documentation for this option
            # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
            system.stateVersion = "23.10"; # Did you read the comment?
	  })
	];
      };


    };
}

